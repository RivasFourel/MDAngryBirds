package main;

import javax.swing.SwingUtilities;

import mdab.controler.LevelManager;
import mdab.view.GameDisplayer;
import mdab.view.mainmenu.MainMenuDisplayer;


public class Launcher {

    public static void main(String[] args) {
        
    	LevelManager.getInstance().init();
    	
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	GameDisplayer.getInstance().createFrame();
            	GameDisplayer.getInstance().initAndShow(new MainMenuDisplayer());
            }
        });
    }
}
