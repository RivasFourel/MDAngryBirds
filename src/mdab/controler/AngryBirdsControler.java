package mdab.controler;

import mdab.model.Level;
import mdab.model.PlayerState;
import mdab.model.SpriteState;
import mdab.model.birds.Bird;
import mdab.model.enemies.Pig;
import mdab.view.GameDisplayer;
import mdab.view.LevelDisplayer;

public class AngryBirdsControler {

	public static AngryBirdsControler INSTANCE = null;
	
	public static AngryBirdsControler getInstance() {
		
		if(INSTANCE == null) {
			INSTANCE = new AngryBirdsControler();
		}
		return INSTANCE;
	}
	
    public void changeBirdVelocity(Level ctx, double mouseX, double mouseY) {
        ctx.getBirds().get(ctx.getReadyToShoot()).changeAmmoVelocity(ctx, mouseX, mouseY);
        ctx.getBirds().get(ctx.getReadyToShoot()).setStatus(SpriteState.MOVING);
    }
    
    public void initLevel(Level ctx){
        ctx.getBirds().get(0).setPositionX(105);
        ctx.getBirds().get(0).setPositionY(390);
        if(ctx.getBirds().size() > 1){
            for (int i = 1; i < ctx.getBirds().size(); i++) {
                ctx.getBirds().get(i).setPositionX(40 - (i*10));
                ctx.getBirds().get(i).setPositionY(440);
            }
        }
    }
    
    public void resetContext(Level ctx){
        ctx.setReadyToShoot(0);
        for (Bird b : ctx.getBirds()) {
            b.reset();
        }
        for (Pig p : ctx.getPigs()) {
            p.reset();
        }
        ctx.setPlayerState(PlayerState.SELECTING);
        ctx.setScore(0);
        ctx.setDeadPigs(0);
        initLevel(ctx);
    }
    
    public void loadNextLevel(){
    	GameDisplayer.getInstance().changeComponent(new LevelDisplayer(LevelManager.getInstance().getLevelByNumber(LevelManager.getInstance().getCurrentLvl() + 1)));
    }

    public void loadBird(Level ctx) {
        ctx.setPlayerState(PlayerState.SELECTING);
        ctx.getBirds().get(ctx.getReadyToShoot()).setVelocityX(0);
        ctx.getBirds().get(ctx.getReadyToShoot()).setVelocityY(0);
        ctx.getBirds().get(ctx.getReadyToShoot()).setPositionX(105);
        ctx.getBirds().get(ctx.getReadyToShoot()).setPositionY(390);
    }

    public void incrementScore(Level ctx) {
        ctx.setScore(ctx.getScore()+1);
    }

    public void changeBirdPosition(Level ctx) {
        ctx.getBirds().get(ctx.getReadyToShoot()).changeAmmoPosition(ctx);
    }

    public void nextBirdToShoot(Level ctx){
        ctx.setReadyToShoot(ctx.getReadyToShoot() + 1);
        if(ctx.getReadyToShoot() == ctx.getBirds().size())
            AngryBirdsControler.getInstance().changePlayerState(ctx, PlayerState.GAMEOVER);
        else
            AngryBirdsControler.getInstance().loadBird(ctx);
    }
    
    public void collideBird(Level ctx){
        ctx.getBirds().get(ctx.getReadyToShoot()).collide(ctx);
    }
    
    public void killPig(Level ctx,Pig p){
        ctx.getPigs().get(ctx.getPigs().indexOf(p)).setStatus(SpriteState.DEAD);
        ctx.setDeadPigs(ctx.getDeadPigs() + 1);
    }
    
    public void hitPig(Level ctx,Pig p){
        p.setLife(p.getLife() - 1);
        if(p.getLife() < 1)
            killPig(ctx, p);
    }
    
    public void changePlayerState(Level ctx, PlayerState s){
        ctx.setPlayerState(s);
    }
}
