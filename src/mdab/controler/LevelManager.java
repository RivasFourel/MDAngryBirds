package mdab.controler;

import java.util.ArrayList;

import mdab.factory.BirdFactory;
import mdab.model.Level;
import mdab.model.birds.Bird;
import mdab.model.birds.BirdTypes;
import mdab.model.enemies.Pig;
import mdab.tools.FileReader;
import mdab.tools.IllegalLevelNumber;
import mdab.tools.JDOMParser;
import mdab.tools.PigData;
import mdab.tools.SpriteSheetReader;
import mdab.tools.XMLParser;

public class LevelManager {

	private static LevelManager INSTANCE = null;

	private ArrayList<Level> LevelsArray;
	private int currentLvl;
	
	public LevelManager () {
		LevelsArray = new ArrayList<>();
	}

	public static LevelManager getInstance () {

		if (INSTANCE == null) {
			INSTANCE = new LevelManager();
		}

		return INSTANCE;

	}

	public Level getLevelByNumber(int index) {
		currentLvl = index;
		return LevelsArray.get(index -1);
	}

	public int getCurrentLvl() {
		return currentLvl;
	}

	public ArrayList<Level> getLevelsArray() {
		return LevelsArray;
	}

	public void init() {
		XMLParser parser = new JDOMParser();
		SpriteSheetReader reader = new FileReader();

		if (parser.getLevelCount() > 0) {

			for (int i=1; i<=parser.getLevelCount(); i++) {

				Level level = null;
				ArrayList<Pig> pigSet = new ArrayList<>();
				ArrayList<Bird> birdSet = new ArrayList<>();
				ArrayList<PigData> pigArray = new ArrayList<>();
				ArrayList<String> birdArray = new ArrayList<>();

				try {
					pigArray = parser.getPigsByLevelNumber(i);
				} catch (IllegalLevelNumber e) {
					e.printStackTrace();
				}

				for (PigData p : pigArray) {
					Pig pig = new Pig(p.getX(), p.getY(), p.getLife());
					pig.setType(p.getType());
					pigSet.add(pig);
				}

				try {
					birdArray = parser.getBirdsByLevelNumber(i);
				} catch (IllegalLevelNumber e) {
					e.printStackTrace();
				}

				for (String b : birdArray) {
					for (BirdTypes bType : BirdTypes.values()) {
						if(b.equalsIgnoreCase(bType.name())){
							Bird bird = BirdFactory.createBird(bType.getType());
							birdSet.add(bird);
						}
					}
				}

				try {
					level = new Level(("Level " + i), parser.getGravityByLevelNumber(i), pigSet, birdSet);
				} catch (IllegalLevelNumber e) {
					e.printStackTrace();
				}

				level.setBackground(reader.getLevelBackgroungImage());

				LevelsArray.add(level);
			}
		}
	}
}
