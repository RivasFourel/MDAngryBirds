package mdab.controler;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import mdab.model.Level;
import mdab.model.birds.Bird;
import mdab.model.birds.BirdTypes;
import mdab.model.enemies.Pig;
import mdab.model.enemies.PigTypes;
import mdab.tools.FileReader;
import mdab.tools.SpriteSheetReader;

public class SpriteManager {

	private static SpriteManager INSTANCE;
	private HashMap<String, BufferedImage[]> spriteMap = new HashMap<>();

	private SpriteManager(){
		init();
	}

	public static SpriteManager getInstance(){
		if(INSTANCE == null)
			INSTANCE = new SpriteManager();
		return INSTANCE;
	}

	public void init(){
		SpriteSheetReader reader = new FileReader();
		spriteMap.put("mainMenu", reader.getMenuImage());
		spriteMap.put("slingshot", reader.getSlingshotImage());
		for (PigTypes pType : PigTypes.values())
			spriteMap.put(pType.name(), reader.getPigImage(pType));
		for (BirdTypes bType : BirdTypes.values())
			spriteMap.put(bType.getType(), reader.getBirdImage(bType));
	}

	/**The sprites for the main menu.
	 * 
	 * @return [0] = Play, [1] = Load, [2] = Exit, [3] = Background
	 */
	public BufferedImage[] getMainMenuSprites() {
		return spriteMap.get("mainMenu");
	}

	public void renderBackSlingshot(Graphics2D g){
		g.drawImage(spriteMap.get("slingshot")[0], 100, 375, null);
	}

	public void renderFrontSlingshot(Graphics2D g){
		g.drawImage(spriteMap.get("slingshot")[1], 95, 373, null);
	}

	public void renderSprites(Level ctx, Graphics2D g){
		for (Pig p : ctx.getPigs()) {
			p.render(g);
		}

		for (Bird b : ctx.getBirds()) {
			b.render(g);
		}
	}

	public void renderAim(Level ctx, Graphics2D g, int mouseX, int mouseY){
		g.setColor(Color.RED);
		g.drawLine( (int) ctx.getBirds().get(ctx.getReadyToShoot()).getPositionX(), 
				(int) ctx.getBirds().get(ctx.getReadyToShoot()).getPositionY(),
				mouseX , mouseY);
	}

	public HashMap<String, BufferedImage[]> getSpriteMap() {
		return spriteMap;
	}
}
