package mdab.controler;

import mdab.model.Level;
import mdab.model.PlayerState;
import mdab.model.SpriteState;
import mdab.model.birds.Bird;
import mdab.model.enemies.Pig;

public class PhysicsManager {
    
    private static PhysicsManager INSTANCE = null;
    private Level currentContext;
    private boolean collide;
    
    private PhysicsManager(){
        collide = false;
    }
    
    public static PhysicsManager getInstance(){
        if(INSTANCE == null)
            INSTANCE = new PhysicsManager();
        return INSTANCE;
    }
    
    // Calcule la distance entre deux points
    private static double distance(double x1, double y1, double x2, double y2) {
        double dx = x1 - x2;
        double dy = y1 - y2;
        return Math.sqrt(dx * dx + dy * dy);
    }
    
    private static int sizeImpact(Bird b, Pig p){
        int sPig = (int) SpriteManager.getInstance().getSpriteMap().get(p.getType())[0].getHeight() / 3;
        int sBird = (int) SpriteManager.getInstance().getSpriteMap().get(b.getClass().getSimpleName())[0].getHeight() / 3;
        return sBird + sPig;
    }
    
    public void processPhysic(Level ctx){
        currentContext = ctx;
        Bird shootedBird = currentContext.getBirds().get(currentContext.getReadyToShoot());
        AngryBirdsControler.getInstance().changeBirdPosition(currentContext);
        
      //Si entre en contact avec un des cochons
      for (Pig pig : currentContext.getPigs()) {
          if(distance(shootedBird.getPositionX(),shootedBird.getPositionY(),
                  pig.getPositionX(),pig.getPositionY()) < sizeImpact(shootedBird, pig)
                  && pig.getStatus().equals(SpriteState.IDLE)) {
              AngryBirdsControler.getInstance().collideBird(currentContext);
              AngryBirdsControler.getInstance().incrementScore(currentContext);
              AngryBirdsControler.getInstance().hitPig(currentContext, pig);
              
              if(currentContext.getPigs().size() == currentContext.getDeadPigs()){
                  AngryBirdsControler.getInstance().changePlayerState(currentContext, PlayerState.VICTORY);
                  for (int i = ctx.getReadyToShoot(); i < ctx.getBirds().size(); i++) {
                      AngryBirdsControler.getInstance().incrementScore(currentContext);
                }
              }
          }
      }
        
        //Si rate la cible
        if(shootedBird.getPositionX() < 20 || shootedBird.getPositionX() > 780 ||
              shootedBird.getPositionY() < 0 || shootedBird.getPositionY() > 460) {
            AngryBirdsControler.getInstance().collideBird(currentContext);
        }
    }

    public boolean isCollide() {
        return collide;
    }

    public void setCollide(boolean collide) {
        this.collide = collide;
    }
}
