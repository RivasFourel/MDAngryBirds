package mdab.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import mdab.model.birds.Bird;

public class BirdFactory {
    
    private final static String PACKAGE = "mdab.model.birds.";
    
    public static Bird createBird(String className) {
        String fullName = PACKAGE + className;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(fullName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Constructor<?> ctor = null;
        try {
            ctor = clazz.getConstructor();
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }
        Object object = null;
        try {
            object = ctor.newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        return (Bird) object;
    }

}
