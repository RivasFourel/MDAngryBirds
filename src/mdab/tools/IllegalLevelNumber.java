package mdab.tools;

public class IllegalLevelNumber extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1302898706034764103L;

	public IllegalLevelNumber(int lvlNbr) {
		super("The level number " + lvlNbr + " is invalid. Check in res/XML/levels.xml if level number " + lvlNbr + " exists and if it is correctly implemented.");
	}

}
