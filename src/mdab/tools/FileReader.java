package mdab.tools;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mdab.model.birds.BirdTypes;
import mdab.model.enemies.PigTypes;

public class FileReader implements SpriteSheetReader {
    
    public FileReader(){}

    public BufferedImage[] shredSpriteSheet(String filename, int rows, int cols, int dim) throws IOException{
        BufferedImage bigImg = ImageIO.read(new File(filename));
        final int width = dim;
        final int height = dim;
        BufferedImage[] sprites = new BufferedImage[rows * cols];

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                sprites[(i * cols) + j] = bigImg.getSubimage(
                    j * width,
                    i * height,
                    width,
                    height
                );
            }
        }
        
        return sprites;
       }

    @Override
    public BufferedImage readSpriteImage(String filename) {
        BufferedImage buff = null;
        try {
            buff = ImageIO.read(new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buff;
    }

    @Override
    public BufferedImage[] getMenuImage() {
        BufferedImage[] mainMenuSprites = new BufferedImage [4];
        mainMenuSprites[0] = this.readSpriteImage("res/buttons/play.png");
        mainMenuSprites[1] = this.readSpriteImage("res/buttons/charger.png");
        mainMenuSprites[2] = this.readSpriteImage("res/buttons/quitter.png");
        mainMenuSprites[3] = this.readSpriteImage("res/img/background.png");
        return mainMenuSprites;
    }

    @Override
    public BufferedImage[] getSlingshotImage() {
        BufferedImage[] slingShotSprites = new BufferedImage [2];
        slingShotSprites[0] = this.readSpriteImage("res/sprites/slingshot_back.png");
        slingShotSprites[1] = this.readSpriteImage("res/sprites/slingshot_front.png");
        return slingShotSprites;
    }

    @Override
    public BufferedImage[] getPigImage(PigTypes type) {
        BufferedImage[] pigSprites = new BufferedImage [type.getRow()*type.getCol()];
        try {
            pigSprites = this.shredSpriteSheet(
                    "res/sprites/pig/"+type.getType()+".png",
                    type.getRow(),
                    type.getCol(),
                    type.getSize());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pigSprites;
    }

    @Override
    public BufferedImage[] getBirdImage(BirdTypes type) {
        BufferedImage[] birdSprites = new BufferedImage [type.getRow()*type.getCol()];
        try {
            birdSprites = this.shredSpriteSheet(
                    "res/sprites/bird/"+type.getType()+".png",
                    type.getRow(),
                    type.getCol(),
                    type.getSize());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return birdSprites;
    }

    @Override
    public BufferedImage getLevelBackgroungImage() {
        return this.readSpriteImage("res/img/background.png");
    }
}
