package mdab.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class JDOMParser implements XMLParser{

	private SAXBuilder sb;
	private Document document;
	@SuppressWarnings("rawtypes")
	private List lvlList;

	public JDOMParser() {
		sb = new SAXBuilder();
		try {
			document = sb.build(new File("res/XML/levels.xml"));
		} catch (JDOMException | IOException e) {
			e.printStackTrace();
		}
		lvlList = document.getRootElement().getChildren();
	}

	@Override
	public int getLevelCount() {
		return lvlList.size();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public double getGravityByLevelNumber(int nbr) throws IllegalLevelNumber {
		double value = -1000;
		Iterator i = lvlList.iterator();
		
		while(i.hasNext())
		{
			Element currentLvl = (Element)i.next();
			
			if (currentLvl.getChild("number").getText().equalsIgnoreCase("" + nbr)) {
				value = Double.parseDouble(currentLvl.getChild("gravity").getText());
			} 
		}

		if( value == -1000) {
			throw new IllegalLevelNumber(nbr);
		}

		return value;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ArrayList<String> getBirdsByLevelNumber(int nbr) throws IllegalLevelNumber {
		ArrayList<String> birdArray = new ArrayList<>();
		Iterator i = lvlList.iterator();

		while(i.hasNext())
		{
			Element currentLvl = (Element)i.next();

			if (currentLvl.getChild("number").getText().equalsIgnoreCase("" + nbr)) {
				List birdList = currentLvl.getChild("birds").getChildren("bird");
				Iterator j = birdList.iterator();

				while(j.hasNext()) {
					Element currentBird = (Element)j.next();
					birdArray.add(currentBird.getChild("type").getText());
				}
			} 
		}

		if (birdArray.isEmpty()) {
			throw new IllegalLevelNumber(nbr);
		}

		return birdArray;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ArrayList<PigData> getPigsByLevelNumber(int nbr) throws IllegalLevelNumber {
		ArrayList<PigData> pigArray = new ArrayList<>();

		Iterator i = lvlList.iterator();

		while(i.hasNext())
		{
			Element currentLvl = (Element)i.next();

			if (currentLvl.getChild("number").getText().equalsIgnoreCase("" + nbr)) {
				List pigList = currentLvl.getChild("pigs").getChildren("pig");
				Iterator j = pigList.iterator();

				while(j.hasNext()) {
					Element currentPig = (Element)j.next();
					String type = currentPig.getChild("type").getText();
					int x = Integer.parseInt(currentPig.getChild("x").getText());
					int y = Integer.parseInt(currentPig.getChild("y").getText());
					int life = Integer.parseInt(currentPig.getChild("life").getText());
					pigArray.add(new PigData(type, x, y, life));
				}
			} 
		}

		if (pigArray.isEmpty()) {
			throw new IllegalLevelNumber(nbr);
		}

		return pigArray;
	}
}
