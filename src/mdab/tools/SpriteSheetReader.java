package mdab.tools;

import java.awt.image.BufferedImage;
import java.io.IOException;

import mdab.model.birds.BirdTypes;
import mdab.model.enemies.PigTypes;

public interface SpriteSheetReader {
    
    public BufferedImage[] shredSpriteSheet(String filename, int rows, int cols, int dim) throws IOException;
    public BufferedImage readSpriteImage(String filename);
    public BufferedImage[] getMenuImage();
    public BufferedImage[] getSlingshotImage();
    public BufferedImage[] getPigImage(PigTypes type);
    public BufferedImage[] getBirdImage(BirdTypes type);
    public BufferedImage getLevelBackgroungImage();
}
