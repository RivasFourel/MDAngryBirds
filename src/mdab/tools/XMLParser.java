package mdab.tools;

import java.util.ArrayList;

public interface XMLParser {

	public int getLevelCount();
	public double getGravityByLevelNumber (int nbr) throws IllegalLevelNumber;
	public ArrayList<String> getBirdsByLevelNumber(int nbr) throws IllegalLevelNumber;
	public ArrayList<PigData> getPigsByLevelNumber(int nbr) throws IllegalLevelNumber;
	
}
