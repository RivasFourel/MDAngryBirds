package mdab.tools;

public class PigData {

	private int x,y,life;
	private String type;
	
	
	public PigData(String type, int x, int y, int life) {
		this.type = type;
		this.x = x;
		this.y = y;
		this.life = life;
	}


	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getLife() {
		return life;
	}

	public String getType() {
		return type;
	}
}
