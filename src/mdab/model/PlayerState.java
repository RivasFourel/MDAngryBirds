package mdab.model;

public enum PlayerState {

    SELECTING,
    FIRING,
    VICTORY,
    GAMEOVER;
}
