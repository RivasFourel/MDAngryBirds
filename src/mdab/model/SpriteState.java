package mdab.model;

public enum SpriteState {
    
    IDLE,
    MOVING,
    DYING,
    DEAD;

}
