package mdab.model.enemies;

public enum PigTypes {
    
    PIG("pig",1,5,48),
    SOLDIERPIG("soldierpig",1,5,86);
    
    private String type;
    private int row, col;
    private int size;
    
    PigTypes(String _type, int _row, int _col, int _size){
        this.type = _type;
        this.row = _row;
        this.col = _col;
        this.size = _size;
    }

    public String getType() {
        return type;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getSize() {
        return size;
    }
}
