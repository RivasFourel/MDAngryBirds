package mdab.model.enemies;

import java.awt.Graphics2D;

import mdab.controler.SpriteManager;
import mdab.model.Sprite;
import mdab.model.SpriteState;

public class Pig extends Sprite{

    private int life;
    private int maxLife;
    private String type;
    
    public Pig(double positionX, double positionY, int maxLife) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.life = maxLife;
        this.maxLife = this.life;
        this.status = SpriteState.IDLE;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMaxLife() {
        return maxLife;
    }

    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void render(Graphics2D g) {
        if(this.status.equals(SpriteState.IDLE)){
            g.drawImage(
                    SpriteManager.getInstance().getSpriteMap().get(this.type)[this.maxLife-this.life],
                    null,
                    (int) this.positionX - 20, (int) this.positionY - 20);
        }
        else if(this.status.equals(SpriteState.DEAD)){
            g.drawImage(
                    SpriteManager.getInstance().getSpriteMap().get(this.type)[4],
                    null,
                    (int) this.positionX - 20, (int) this.positionY - 20);
        }
        
    }

    @Override
    public void reset() {
        this.life = this.maxLife;
        this.status = SpriteState.IDLE;
    }
}
