package mdab.model;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import mdab.model.birds.Bird;
import mdab.model.enemies.Pig;

public class Level {
    
    private String name;
    private double gravity;
    private BufferedImage background;
    
    private ArrayList<Pig> pigs;
    private ArrayList<Bird> birds;
    private int readyToShoot;
    private int deadPigs;
    
    private PlayerState playerState;
    private int score;
    
    public Level(String name, double gravity, ArrayList<Pig> pigs, ArrayList<Bird> birds) {
        this.name = name;
        this.gravity = gravity;
        this.pigs = pigs;
        this.birds = birds;
        this.playerState = PlayerState.SELECTING;
        this.score = 0;
        this.readyToShoot = 0;
        this.deadPigs = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGravity() {
        return gravity;
    }

    public void setGravity(double gravity) {
        this.gravity = gravity;
    }

    public ArrayList<Pig> getPigs() {
        return pigs;
    }

    public void setPigs(ArrayList<Pig> pigs) {
        this.pigs = pigs;
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public ArrayList<Bird> getBirds() {
        return birds;
    }

    public void setBirds(ArrayList<Bird> birds) {
        this.birds = birds;
    }

    public int getReadyToShoot() {
        return readyToShoot;
    }

    public void setReadyToShoot(int readyToShoot) {
        this.readyToShoot = readyToShoot;
    }

    public int getDeadPigs() {
        return deadPigs;
    }

    public void setDeadPigs(int deadPigs) {
        this.deadPigs = deadPigs;
    }

    public BufferedImage getBackground() {
        return background;
    }

    public void setBackground(BufferedImage backgroundName) {
        this.background = backgroundName;
    }
    
}
