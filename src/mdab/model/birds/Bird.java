package mdab.model.birds;

import java.awt.Graphics2D;

import mdab.controler.AngryBirdsControler;
import mdab.controler.PhysicsManager;
import mdab.controler.SpriteManager;
import mdab.model.Level;
import mdab.model.Sprite;
import mdab.model.SpriteState;

public class Bird extends Sprite implements Ammunition{

    private double velocityX;
    private double velocityY;
    
    public Bird(){
        this.velocityX = 0;
        this.velocityY = 0;
        this.status = SpriteState.IDLE;
    }

    public double getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(double velocityX) {
        this.velocityX = velocityX;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(double velocityY) {
        this.velocityY = velocityY;
    }

    @Override
    public void render(Graphics2D g) {
        if(this.status.equals(SpriteState.IDLE)){
            g.drawImage(
                    SpriteManager.getInstance().getSpriteMap().get(this.getClass().getSimpleName())[1],
                    null,
                    (int) this.positionX - 20, (int) this.positionY - 20);  
        }
        else if(this.status.equals(SpriteState.MOVING)){
            g.drawImage(
                    SpriteManager.getInstance().getSpriteMap().get(this.getClass().getSimpleName())[3],
                    null,
                    (int) this.positionX - 20, (int) this.positionY - 20);
        }
        else if(this.status.equals(SpriteState.DEAD)){
            g.drawImage(
                    SpriteManager.getInstance().getSpriteMap().get(this.getClass().getSimpleName())[4],
                    null,
                    (int) this.positionX - 20, (int) this.positionY - 20);
        }
        
    }

    @Override
    public void collide(Level ctx) {
        PhysicsManager.getInstance().setCollide(true);
        this.status = SpriteState.DEAD;
        AngryBirdsControler.getInstance().nextBirdToShoot(ctx);
    }

    @Override
    public void reset() {
        this.status = SpriteState.IDLE;
    }

    @Override
    public void changeAmmoVelocity(Level ctx, double mouseX, double mouseY) {
        this.setVelocityX((ctx.getBirds().get(ctx.getReadyToShoot()).getPositionX() - mouseX) / 20.0);
        this.setVelocityY((ctx.getBirds().get(ctx.getReadyToShoot()).getPositionY() - mouseY) / 20.0);
        
    }

    @Override
    public void changeAmmoPosition(Level ctx) {
        this.setPositionX(ctx.getBirds().get(ctx.getReadyToShoot()).getPositionX() + ctx.getBirds().get(ctx.getReadyToShoot()).getVelocityX());
        this.setPositionY(ctx.getBirds().get(ctx.getReadyToShoot()).getPositionY() + ctx.getBirds().get(ctx.getReadyToShoot()).getVelocityY());
        this.setVelocityY(ctx.getBirds().get(ctx.getReadyToShoot()).getVelocityY() + ctx.getGravity());
    }

}
