package mdab.model.birds;

import mdab.controler.AngryBirdsControler;
import mdab.controler.PhysicsManager;
import mdab.model.Level;
import mdab.model.SpriteState;

public class BigBird extends Bird implements Ammunition {

    private int bounceLeft;
    
    public BigBird(){
        super();
        this.bounceLeft = 4;
    }
    
    public int getBounceLeft() {
        return bounceLeft;
    }

    public void setBounceLeft(int bounceLeft) {
        this.bounceLeft = bounceLeft;
    }
    
    @Override
    public void reset(){
        this.status = SpriteState.IDLE;
        this.bounceLeft = 4;
    }

    @Override
    public void collide(Level ctx){
        bounceLeft --;
        if(bounceLeft > 0){
            this.setVelocityX(this.getVelocityX() * (0.5));
            this.setVelocityY(this.getVelocityY() * (-0.8));
        }
        else{
            PhysicsManager.getInstance().setCollide(true);
            this.status = SpriteState.DEAD;
            AngryBirdsControler.getInstance().nextBirdToShoot(ctx);
        }
            
    }
    
    @Override
    public void changeAmmoPosition(Level ctx) {
        this.setPositionX(ctx.getBirds().get(ctx.getReadyToShoot()).getPositionX() + ctx.getBirds().get(ctx.getReadyToShoot()).getVelocityX());
        this.setPositionY(ctx.getBirds().get(ctx.getReadyToShoot()).getPositionY() + ctx.getBirds().get(ctx.getReadyToShoot()).getVelocityY());
        this.setVelocityY(ctx.getBirds().get(ctx.getReadyToShoot()).getVelocityY() + (ctx.getGravity()*1.8));
    }
}
