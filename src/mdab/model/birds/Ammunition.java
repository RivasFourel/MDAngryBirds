package mdab.model.birds;

import mdab.model.Level;

public interface Ammunition {
    
    public void collide(Level ctx);
    public void changeAmmoVelocity(Level ctx,  double mouseX, double mouseY);
    public void changeAmmoPosition(Level ctx); 
}
