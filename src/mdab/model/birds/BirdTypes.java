package mdab.model.birds;

public enum BirdTypes {
   
    BIRD("Bird",1,5,48),
    BIGBIRD("BigBird",1,5,54);
    
    private String type;
    private int row, col;
    private int size;
    
    BirdTypes(String _type, int _row, int _col, int _size){
        this.type = _type;
        this.row = _row;
        this.col = _col;
        this.size= _size;
    }

    public String getType() {
        return type;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getSize() {
        return size;
    }
}
