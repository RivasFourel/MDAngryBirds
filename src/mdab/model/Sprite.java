package mdab.model;

import java.awt.Graphics2D;

public abstract class Sprite {

	protected SpriteState status;
	protected double positionX;
	protected double positionY;

	public abstract void render(Graphics2D graphics);

	public abstract void reset();

	public SpriteState getStatus() {
		return status;
	}

	public void setStatus(SpriteState status) {
		this.status = status;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}
}
