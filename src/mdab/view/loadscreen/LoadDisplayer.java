package mdab.view.loadscreen;

import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import mdab.controler.LevelManager;
import mdab.controler.SpriteManager;
import mdab.view.buttons.LevelButton;

public class LoadDisplayer extends JPanel{

	private static final long serialVersionUID = -4492783316278665176L;

	public LoadDisplayer() {
		
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.insets = new Insets(20, 20, 20, 20);
		setLayout(layout);
		for (int i=1; i<=LevelManager.getInstance().getLevelsArray().size(); i++) {
			LevelButton button = new LevelButton(i);
			add(button,constraints);
		}
		
	}

	public void paintComponent(Graphics g) {
		g.drawImage(SpriteManager.getInstance().getMainMenuSprites()[3], 0, 0, null);
	}
	
}
