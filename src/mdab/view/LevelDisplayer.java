package mdab.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import mdab.controler.AngryBirdsControler;
import mdab.controler.PhysicsManager;
import mdab.controler.SpriteManager;
import mdab.model.Level;
import mdab.model.PlayerState;

public class LevelDisplayer extends Panel implements Runnable, MouseListener, MouseMotionListener {
    private static final long serialVersionUID = 1L;

	private Level context;
    
    private int mouseX, mouseY;                         // position de la souris lors de la sélection
    private Graphics2D graph;
    private Image buffer;                               // image pour le rendu hors écran
    
    public LevelDisplayer(Level _contexte){
        this.context = _contexte;
        addMouseListener(this);
        addMouseMotionListener(this);
        
        initLevel();
    }
    
    public void initLevel(){
        AngryBirdsControler.getInstance().initLevel(context);
    }
    
    // Gestion des événements souris
    public void mouseClicked(MouseEvent e) { }
    public void mouseEntered(MouseEvent e) { }
    public void mouseExited(MouseEvent e) { }
    public void mousePressed(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) {
        switch (context.getPlayerState()) {
            case SELECTING:
                AngryBirdsControler.getInstance().changeBirdVelocity(context,mouseX,mouseY);
                AngryBirdsControler.getInstance().changePlayerState(context, PlayerState.FIRING);
                new Thread(this).start();
                break;
            case GAMEOVER:
                AngryBirdsControler.getInstance().resetContext(context);
                break;
            case VICTORY:
                AngryBirdsControler.getInstance().loadNextLevel();
                break;
            default:
                break;
        }
        repaint();
    }
    public void mouseDragged(MouseEvent e) { mouseMoved(e); }
    public void mouseMoved(MouseEvent e) { 
        mouseX = e.getX();
        mouseY = e.getY();
        repaint();
    }

    @Override
    public void run() {
        while(!PhysicsManager.getInstance().isCollide()){
            // un pas de simulation toutes les 10ms
            try { Thread.sleep(10); } catch(InterruptedException e) { }
            PhysicsManager.getInstance().processPhysic(context);
            repaint();
        }
        PhysicsManager.getInstance().setCollide(false);
    }

    // Evite les scintillements
    public void update(Graphics g) {
        paint(g);
    }

    // Dessine le contenu de l'écran dans un buffer puis copie le buffer à l'écran
    public void paint(Graphics g2) {
        if(buffer == null) buffer = createImage(800, 600);        
        graph = (Graphics2D) buffer.getGraphics();
        
        // Fond
        graph.drawImage(context.getBackground(), 0, 0, null);
        
        //Ligne de visée
        if(context.getPlayerState() == PlayerState.SELECTING){
            SpriteManager.getInstance().renderAim(context, graph, mouseX, mouseY);
        }
        
        SpriteManager.getInstance().renderBackSlingshot(graph);
        SpriteManager.getInstance().renderSprites(context, graph);
        SpriteManager.getInstance().renderFrontSlingshot(graph);
        
        //Score
        graph.setColor(Color.BLACK);
//        graph.drawString(message, 300, 100);
        graph.drawString("Score: " + context.getScore(), 20, 20);

        //Affichage à l'écran sans scintillement
        g2.drawImage(buffer, 0, 0, null);
    }

    //Taille de la fenêtre
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }
}
