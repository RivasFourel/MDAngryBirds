package mdab.view.mainmenu;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import mdab.controler.SpriteManager;
import mdab.view.buttons.Button;
import mdab.view.buttons.ExitButton;
import mdab.view.buttons.LoadButton;
import mdab.view.buttons.PlayButton;


public class MainMenuDisplayer extends JPanel{

	private static final long serialVersionUID = 1L;

	public MainMenuDisplayer () {
		super();

		Button playButton = new PlayButton(new ImageIcon(SpriteManager.getInstance().getMainMenuSprites()[0]));
		Button loadButton = new LoadButton(new ImageIcon(SpriteManager.getInstance().getMainMenuSprites()[1]));
		Button exitButton = new ExitButton(new ImageIcon(SpriteManager.getInstance().getMainMenuSprites()[2]));

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		//--
		add(playButton);
		playButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		//--
		add(Box.createVerticalStrut(50));
		//--
		add(loadButton);
		loadButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		//--
		add(Box.createVerticalStrut(20));
		//--
		add(exitButton);
		exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(SpriteManager.getInstance().getMainMenuSprites()[3], 0, 0, null);
	}

}
