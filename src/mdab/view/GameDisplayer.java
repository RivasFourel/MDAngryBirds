package mdab.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameDisplayer {
    
    private static GameDisplayer INSTANCE;
    private Frame window;
    
    private GameDisplayer() {}
    
    public static GameDisplayer getInstance(){
        if(INSTANCE == null)
            INSTANCE = new GameDisplayer();
        return INSTANCE;
    } 
    
    public void createFrame(){
        window = new Frame("Angry Birds");
        window.setResizable(false);
        window.setSize(new Dimension(800,600));
        window.setPreferredSize(new Dimension(800, 600));
        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                System.exit(0);
            }
        });
    }
    
    public void initAndShow(Component game){
        window.add(game);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
    
    public void changeComponent (Component component) {
    	window.removeAll();
    	window.add(component);
    	window.revalidate();
    	window.setLocationRelativeTo(null);
    }
    
}