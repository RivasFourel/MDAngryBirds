package mdab.view.buttons;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

public class ExitButton extends Button {
	

	private static final long serialVersionUID = -5085642125886883574L;

	public ExitButton(ImageIcon icon) {
		super(icon);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.exit(0);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

}
