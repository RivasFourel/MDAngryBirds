package mdab.view.buttons;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public abstract class Button extends JButton implements MouseListener{
	
	private static final long serialVersionUID = 1L;

	public Button () {
		super();
		addMouseListener(this);
	}
	
	public Button (ImageIcon icon) {
		super(icon);
		setBorder(BorderFactory.createEmptyBorder());
		setContentAreaFilled(false);
		enableInputMethods(true);   
		addMouseListener(this);
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		setCursor(new Cursor(Cursor.HAND_CURSOR));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	@Override
	public abstract void mouseClicked(MouseEvent e);
	
	@Override
	public abstract void mousePressed(MouseEvent e);

	@Override
	public abstract void mouseReleased(MouseEvent e);

}
