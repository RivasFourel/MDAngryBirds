package mdab.view.buttons;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import mdab.view.GameDisplayer;
import mdab.view.loadscreen.LoadDisplayer;

public class LoadButton extends Button {

	private static final long serialVersionUID = 1L;

	public LoadButton(ImageIcon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		GameDisplayer.getInstance().changeComponent(new LoadDisplayer());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
