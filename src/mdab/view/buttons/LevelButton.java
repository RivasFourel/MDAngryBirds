package mdab.view.buttons;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;

import mdab.controler.LevelManager;
import mdab.view.GameDisplayer;
import mdab.view.LevelDisplayer;

public class LevelButton extends Button {

	private static final long serialVersionUID = 8912347782367512980L;

	private int lvlNumber;
	
	public LevelButton(int lvlNumber) {
		super();
		this.lvlNumber = lvlNumber;
		setText("" + lvlNumber);
		setSize(new Dimension(60,60));
		setPreferredSize(new Dimension(60,60));
		setBackground(new Color(255,230,0));
		setForeground(Color.WHITE);
		setFont(new Font("Comic Sans MS",Font.BOLD,35));
		setOpaque(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		GameDisplayer.getInstance().changeComponent(new LevelDisplayer(LevelManager.getInstance().getLevelByNumber(lvlNumber)));
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
