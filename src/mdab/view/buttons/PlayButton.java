package mdab.view.buttons;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import mdab.controler.LevelManager;
import mdab.view.GameDisplayer;
import mdab.view.LevelDisplayer;

public class PlayButton extends Button {

	private static final long serialVersionUID = 1039716898925343599L;

	public PlayButton(ImageIcon icon) {
		super(icon);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		GameDisplayer.getInstance().changeComponent(new LevelDisplayer(LevelManager.getInstance().getLevelByNumber(1)));
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
